// 1.引入express
const express = require('express');

// 2.创建应用对象
const app = express();

// 3.创建路由规则:GET
app.get('/server', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    // 设置响应
    response.send("Hello Ajax");
});

// 3.创建路由规则:POST
// app.post('/server', (request, response) => {
//     // 允许跨域
//     response.setHeader('Access-Control-Allow-Origin','*')
//     // 设置响应
//     response.send("Hello Ajax POST");
// });

// 3.接收任意类型的请求
app.all('/server', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    response.setHeader('Access-Control-Allow-Headers','*')
    // 设置响应
    response.send("Hello Ajax POST");
});

app.all('/json-server', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    response.setHeader('Access-Control-Allow-Headers','*')
    // 响应一个对象数据
    const data = {
        name : 'atguigu'
    }
    // 因为send方法中不能传递对象，所以需要使用JSON方法将对象转换为字符串
    let str = JSON.stringify(data);
    // 设置响应体
    response.send(str);
});

// 针对IE
app.get('/IE', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    // response.setHeader('Access-Control-Allow-Headers','*')
    // 设置响应
    response.send("Hello IE ");
});

// 延时响应
app.get('/delay', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    // response.setHeader('Access-Control-Allow-Headers','*')
    // 设置一个定时器，延时调用
    setTimeout(() => {
        // 设置响应
        response.send("延时响应");
    }, 3000);
    
});

app.all('/jquery-server', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    response.setHeader('Access-Control-Allow-Headers','*')
    // 设置一个定时器，延时调用
    // setTimeout(() => {
    //     // 设置响应
    //     response.send("延时响应");
    // }, 3000);
    const data = {name: "xiaoming"};
    // setTimeout(() => {
    // }, 3000);
    response.send(JSON.stringify(data));
    
});

app.all('/axios-server', (request, response) => {
    // 允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    response.setHeader('Access-Control-Allow-Headers','*')
    // 设置一个定时器，延时调用
    // setTimeout(() => {
    //     // 设置响应
    //     response.send("延时响应");
    // }, 3000);
    const data = {name: "xiaoming"};
    // setTimeout(() => {
    // }, 3000);
    response.send(JSON.stringify(data));
    
});

app.all('/jsonp-server', (request, response) => {
    // 允许跨域
    // response.setHeader('Access-Control-Allow-Origin','*')
    // response.setHeader('Access-Control-Allow-Headers','*')
    const data = {name: "xiaoming"};
    // response.send(JSON.stringify(data));
    let str = JSON.stringify(data);
    // 响应回去的是一个js的代码
    response.end(`handle(${str})`);
    
});

// 检测用户名是否存在
app.all('/check-username', (request, response) => {
    // 允许跨域
    // response.setHeader('Access-Control-Allow-Origin','*')
    // response.setHeader('Access-Control-Allow-Headers','*')
    const data = {
        existl: 1,
        msg: "用户名已经存在"
    };
    // response.send(JSON.stringify(data));
    let str = JSON.stringify(data);
    // 响应回去的是一个js的代码
    response.end(`handle(${str})`);
    
});

app.all('/jquery-jsonp-server', (request, response) => {
    // 允许跨域
    // response.setHeader('Access-Control-Allow-Origin','*')
    // response.setHeader('Access-Control-Allow-Headers','*')
    // response.setHeader('Access-Control-Allow-Method','*')
    const data = {
        name: "尚硅谷",
        city: ["北京", "上海", "广州"]
    };
    // response.send(JSON.stringify(data));
    let str = JSON.stringify(data);
    // 接收callback 参数
    // 这里的cb就是一串字符串，jQuery36007563534601954935_1629725266999
    let cb = request.query.callback;
    // 响应回去的是一个js的代码
    response.end(`${cb}(${str})`);
    
});

// 4.监听端口启动服务
app.listen(8000, () => {
    console.log("服务已经启动，8000 端口监听中 ...");
});