const express = require("express")
const app = express()

app.get("/home", (request, response) => {
    // 当请求到home的时候，响应回一个页面
    response.sendFile(__dirname + "/10-同源策略.html")
})

app.get("/data", (request, response) => {
    // 当请求到home的时候，响应回一个页面
    response.send("用户数据")
})

app.listen(9000, () => {
    console.log("服务已经启动，9000 端口监听中 ...");
})